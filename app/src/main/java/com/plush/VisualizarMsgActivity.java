package com.plush;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

public class VisualizarMsgActivity extends AppCompatActivity {

    final Context context = this;
    TextView msgrecebida;
    ProgressDialog pdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar);

        msgrecebida = (TextView) findViewById(R.id.mensagem_recebida);

        startService(new Intent(VisualizarMsgActivity.this, ServiceMediaPlayer.class));
        startService(new Intent(VisualizarMsgActivity.this, ServiceMap.class));

        pdialog = ProgressDialog.show(context, " Mensagens Recebidas", " Por favor, aguarde ...", true);

        visualizar();

        Intent it = getIntent();
        JSONObject jobj = null;

        try {
            if (jobj == null) {
                return;
            } else {
                jobj = new JSONObject(it.getExtras().getString("com.parse.Data"));
                JSONObject jsonObject = new JSONObject(jobj.get("alert").toString());
                final String id = jsonObject.get("id").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void alertDisplayResponderMensagens() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VisualizarMsgActivity.this)
                //  .setTitle(remetentenome + " , você deseja ... ")
                .setMessage("Responder ? ")
                //   .setIcon(R.drawable.ic_launcher)
                .setNegativeButton(" NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        stopService(new Intent(VisualizarMsgActivity.this, ServiceMediaPlayer.class));
                        stopService(new Intent(VisualizarMsgActivity.this, ServiceMap.class));
                        Intent intent = new Intent(VisualizarMsgActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(), R.anim.fade_in, R.anim.mover_direita);
                        ActivityCompat.startActivity(VisualizarMsgActivity.this, intent, activityOptionsCompat.toBundle());
                    }
                })
                .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = new Intent(VisualizarMsgActivity.this, MensagemActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(), R.anim.fade_in, R.anim.mover_direita);
                        ActivityCompat.startActivity(VisualizarMsgActivity.this, intent, activityOptionsCompat.toBundle());
                    }
                });
        builder.create().show();
    }

    public void visualizar() {
        pdialog.dismiss();
        alertDisplayResponderMensagens();
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Mensagem");
        query.orderByDescending("createdAt");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                String mensagem;
                mensagem = object.get("type").toString();
                mensagem = mensagem.toUpperCase();
                msgrecebida.setText(mensagem);
            }
        });

    }
}

