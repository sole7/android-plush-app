package com.plush;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.parse.SendCallback;

public class MensagemActivity extends AppCompatActivity {
    final Context context = this;
    ProgressDialog pdialog = null;
    EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensagem);

        startService(new Intent(MensagemActivity.this, ServiceMediaPlayer.class));
        startService(new Intent(MensagemActivity.this, ServiceMap.class));

        message = (EditText) findViewById(R.id.pokeText);

        final ImageButton button_mensagem = findViewById(R.id.button_enviar);
        button_mensagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarMsg();
            }
        });

        final ImageButton button_volume = findViewById(R.id.button_volume);
        button_volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(new Intent(MensagemActivity.this, ServiceMediaPlayer.class));
            }
        });

        final ImageButton navegacao_button = findViewById(R.id.navegacao);
        navegacao_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MensagemActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(), R.anim.fade_in, R.anim.mover_esquerda);
                ActivityCompat.startActivity(MensagemActivity.this, intent, activityOptionsCompat.toBundle());
            }
        });

    }

    public void sendParse() {
        try {

            String objectId = null;
            ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
            objectId = currentInstallation.getObjectId();
            ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
            pushQuery.whereNotEqualTo("objectId", objectId);
            Log.i("buz", "sendParse: " + objectId);

            ParsePush push = new ParsePush();
            push.setQuery(pushQuery);
            push.setMessage("{" + "\"msg\":" + "\"Novas mensagem no seu PlushAPP\"" +
                    ",\"id\":" + objectId + ",\"ob\":" + "\"notificacao\"" + "}");
            push.sendInBackground(new SendCallback() {
                @Override
                public void done(ParseException e) {
                    Log.i("zx", "Push is sent!");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void enviarMsg() {
        pdialog = ProgressDialog.show(context, " ", "Por favor, aguarde ...", true);
        final ParseObject poke = new ParseObject("Mensagem");
        if (message.getText().toString().equals("")) {
            pdialog.dismiss();
             Toast.makeText(getApplicationContext(), " Ops !! Você não digitou uma mensagem !", Toast.LENGTH_LONG).show();

        } else {
            poke.put("type", message.getText().toString());
            poke.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    pdialog.dismiss();
                    sendParse();
                    alertDisplayer("", "Mensagem enviada !");
                }
            });
        }
    }

    private void alertDisplayer(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MensagemActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        stopService(new Intent(MensagemActivity.this, ServiceMediaPlayer.class));
                        stopService(new Intent(MensagemActivity.this, ServiceMap.class));
                        Intent intent = new Intent(MensagemActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeCustomAnimation(getApplicationContext(), R.anim.fade_in, R.anim.mover_esquerda);
                        ActivityCompat.startActivity(MensagemActivity.this, intent, activityOptionsCompat.toBundle());
                    }
                });
        AlertDialog ok = builder.create();
        ok.show();
    }
}