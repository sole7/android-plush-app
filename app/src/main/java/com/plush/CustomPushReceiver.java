package com.plush;

import android.content.Context;
import android.content.Intent;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;


public class CustomPushReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected int getSmallIconId(Context context, Intent intent) {
        return (R.mipmap.ic_launcher_yonlu);
    }

    @Override
    protected void onPushOpen(Context c, Intent i) {

        JSONObject jobj = null;
        try {
            jobj = new JSONObject(i.getExtras().getString("com.parse.Data"));
            JSONObject mensagem = new JSONObject(jobj.get("alert").toString());
            if (mensagem.getString("ob").equals("notificacao")) {
                Intent intent = new Intent(c, VisualizarMsgActivity.class);
                intent.putExtras(i.getExtras());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(intent);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        if (intent == null) return;
        super.onPushReceive(context, intent);
    }

}
